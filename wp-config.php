<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
/** WP Memory Limit */
define( 'WP_MEMORY_LIMIT', '256M' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'open_agencia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database passwogulp */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']]}xHZTJ>1k}:dXVw)j2eS/#cJI8l~T:p)WqP+mt^<wC^1Aq-xJc>iK,6y7dO7yI');
define('SECURE_AUTH_KEY',  '}h8]ZXvJzhlTDB vkahmh^2i/b4DjzON0lJI)TLKcR~s.m9=Q`o+Khn&J+aGfuVX');
define('LOGGED_IN_KEY',    'rwp1x/ZkpIAb}=s.ktQ/`4|re6nd.Kp}`][:/PiP2aJ2ij2Nr2r*5g{v;; +cflV');
define('NONCE_KEY',        'cEy25n:?XM=KR78)(8/w*{<W{GL;q4p~OJ-srfgtQ}+NL;pu,w+hr6K#6U[]y}aI');
define('AUTH_SALT',        'NdQ4;542|uiN7vnNznag^.k/|[wEG)$K!< W(6E2w/cinkK7hr%.[*V7L(S>2>$c');
define('SECURE_AUTH_SALT', 'fp#XV3<Q!WMq^ft^&8&<`hfx3n<hcNsH.L`)[KMQdgObAKWV:endIxVpKDktWX>I');
define('LOGGED_IN_SALT',   'pr4eZMz!p5v8-Ge;B]{}*]}Fe4vRMggSP2R#JyIbDh:sok/;wBwuHi3Kk^(~7/7_');
define('NONCE_SALT',       'pMX<y]9nK,[Nj`+oW?kUQ*oZ#7?-qNAw#AT>Yu|V4ym!j4S+Kp}U+<~OiWu`1uTD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


