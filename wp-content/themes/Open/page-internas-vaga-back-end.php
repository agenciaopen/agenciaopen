<?php 

/**
 * The template name: Interna Trabalhe backend
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Desenvolvedor(a) back-end Pleno </h2>
                    
                    <h3>Atividades e Responsabilidades:</h3>
                    <ul>
                        <li><span> - </span>Concilie demandas e prazos comerciais com qualidade;</li>
                        <li><span> - </span>Tenha boa comunicação e trabalho em equipe;</li>
                        <li><span> - </span>Seja proativo trazendo sempre novas ideias para resolvermos nossos desafios;</li>
                         <li><span> - </span>Siga as tendências do mercado e que seja capaz de se adaptar rapidamente a novas tecnologias;</li>                      
                         <li><span> - </span>Tenha senso de organização, atenção a detalhes e foco.</li>                      
                    </ul>
                    <h3>Diferenciais: </h3>
                    <ul>
                        <li>Experiência em Git, asp.net Core.</li>
                    </ul>

                    <h3>Requisitos:</h3>
                    <ul>
                        <li><span> - </span>Desenvolvimento JavaScript;</li>
                        <li><span> - </span>C#;</li>
                        <li><span> - </span>Conhecimento em MVC;</li>
                         <li><span> - </span>SQL Server.</li>                      
                    </ul>


                    <h3>Escolaridade:  </h3>
                    <ul>
                        <li>Graduação completo em Tecnologia em Sistemas, Ciências da Computação ou áreas afins.</li>
                    </ul>

                    <h3>Remuneração:  </h3>
                    <ul>
                        <li>Trabalharemos com pretensão salarial.</li>
                    </ul>


                    <h3>Benefícios:  </h3>
                    <ul>
                        <li>VT, VR, Plano de saúde.</li>
                    </ul>
                    <br>
                    <br>
                    <ul>
                        <li>Os interessados deverão encaminhar o currículo para: recursoshumanos@agenciaopen.com</li>
                    </ul>

                    
                   
                    <h3>Intrépido aventureiro do back-end, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="993" title="Vaga Desenvolvedor back-end"]') ; ?>
                               
                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>
