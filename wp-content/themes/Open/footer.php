      <section class="contato" id="contato">
            <div class="table">
                <div class="vertical">
                    <div class="title">
                        <h2>Vamos tomar um café?</h2>
                        <span>Preencha o formulário e venha bater um papo com nossos especialistas!</span>
                        <hr />
                    </div>
                    <div class="grid-55 prefix-5 tablet-grid-50">

                       <div class="form">
                           <?php echo do_shortcode('[contact-form-7 id="815" title="Contato Rodapé Home 1"]') ; ?>
                        </div>
                    </div>
                    <div class="grid-30 prefix-10 tablet-grid-40 tablet-prefix-10">
                        <div class="localizacao">
                            <div class="campo-end">
                                <i class="icon-local-01"></i>
                                <!--<p style="margin-bottom: 1rem;">Rua Alagoas - 1160 - 6º andar CEP: 30130-160<br />Bairro funcionários</p>
                                <p>Rua Dom Pedro II - 349 - sala 404 CEP: 90550-142<br />Porto Alegre - RS</p>-->
                            </div>
                            <div class="campo-end">
                                <i class="icon-mensagem-01"></i>
                                <p>31 3047.2931 <br /> contato@agenciaopen.com</p>
                            </div>
                        </div>
                        <ul class="social">
                            <li> <a href="https://www.facebook.com/agenciaopen" target="_blank" title="Facebook"><i class="icon-facebook-01"></i></a></li>
                            <li><a href="https://www.instagram.com/agenciaopen_/" title="Instagram" target="_blank"><i class="icon-instagran-01"></i></a></li>
                           
                            <li><a href="https://www.linkedin.com/company/agencia-open-de-internet-e-aplicativos-web-ltda" target="_blank" title="Linkedin"><i class="icon-linkedin-01"></i></a></li>
                        </ul>


                    </div>

                </div>

            </div>
        </section>
        <!-- <div class="mapa">
            <a href="https://www.google.com.br/maps/place/R.+Alagoas,+1160+-+6%C2%BA+andar+-+Funcion%C3%A1rios,+Belo+Horizonte+-+MG/data=!4m2!3m1!1s0xa699da54150531:0xd930567502ec875?sa=X&ved=2ahUKEwjxsqb0xtDaAhWBi5AKHWblDaQQ8gEwAHoECAAQLg" title="" target="_blank">
                <img src="<?php bloginfo('template_url') ?>/build/img/google-maps.jpg" alt="" />
            </a>
        </div> -->
    </main>
    <footer class="footer" id="footer">
        <div class="mobile-grid-70 mobile-prefix-15 mobile-suffix-15">
            <a href="" title="">
            <img src="<?php bloginfo('template_url') ?>/build/img/logo-open-footer.png" alt="" />
        </a>
        </div>

    </footer>
    <a href="" class="icon-whatsapp share-wpp"></a>
        <script>
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            location = 'http://agenciaopen.com/sucesso';
            dataLayer.push({ 'event': 'formulario enviado'});
        }, false );
        </script>

    <?php wp_footer();?>
    <script src="https://apis.google.com/js/platform.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/build/js/libs.min.js"></script>
    <script src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/build/js/script.min.js"></script>
    <script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/c823d753-f69d-46ce-a709-6a93c0196aae-loader.js"></script>


</body>
  </html>

