<?php 

/**
 * The template name: Interna Trabalhe infraestrutura
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Analista de Infraestrutura </h2>
                    
                    <h3>Principais Responsabilidades:</h3>
                    <ul>
                        <li><span> - </span>Definir regras de utilização de sistemas e equipamentos;</li>
                        <li><span> - </span>Controlar os serviços de sistemas operacionais e de banco de dados;</li>
                        <li><span> - </span>Prestar suporte técnico ao usuário (cliente interno e externo quando solicitado);</li>
                        <li><span> - </span>Oferecer soluções para ambientes informatizados;</li>
                        <li><span> - </span>Configuração e Administração de redes e Servidores Linux e Windows Server 2008R2, 2012R2, 2016</li>
                        <li><span> - </span>Implantação e Administração de serviços ADDS, ADFS, ADRMS, ADCS, GPO, File Server;</li>
                        <li><span> - </span>Configuração, Administração e Monitoramento Nuvem (AWS, AZURE);</li>
                        <li><span> - </span>Configuração e Administração Sites .NET, Sharepoint e Wordpress;</li>
                        <li><span> - </span>Administração e criação de sites Sharepoint 2003 e 2016;</li>
                        <li><span> - </span>Administração e instalação de Soluções em virtualização HyperV;</li>
                        <li><span> - </span>Configuração, instalação e Administração de aplicações de monitoramento de redes (PRTG);</li>
                        <li><span> - </span>Configuração e Administração intermediária de banco de dados SQL Server;</li>
                        <li><span> - </span>Configuração e Administração de roteadores, switch e firewall - TCP/IP-DNS-DHCP-VPN-VLAN e outros;</li>
                        <li><span> - </span>Administração, implantação de soluções de segurança de redes;</li>
                        <li><span> - </span>Cabeamento de redes CAT5E 6E e fibra ótica;</li>
                        <li><span> - </span>Montagem e manutenção de Servidores de rack, Blades e rack de telecomunicação;</li>
                        <li><span> - </span>Manutenção e/ou coordenação da terceirização de manutenção de computadores;</li>
                        <li><span> - </span>Gerenciar e monitorar manutenções de rede elétricas, telefônica e de infraestrutura física.</li>
                    </ul>
                    <h3>Requisitos: </h3>
                    <ul>
                        <li><span> - </span>Concilie demandas e prazos comerciais com qualidade;</li>
                        <li><span> - </span>Tenha boa comunicação e trabalho em equipe;</li>
                        <li><span> - </span>Seja proativo trazendo sempre novas ideias para resolvermos nossos desafios;</li>
                        <li><span> - </span>Siga as tendências do mercado e que seja capaz de se adaptar rapidamente a novas tecnologias;</li>
                        <li><span> - </span>Tenha senso de organização, atenção a detalhes e foco.</li>
                    </ul>
                    <h3>Escolaridade:  </h3>
                    <ul>
                        <li>Graduação cursando ou completo em Gestão da Tecnologia da Informação, Tecnologia em Sistemas ou áreas afins.</li>
                    </ul>

                    <h3>Remuneração:  </h3>
                    <ul>
                        <li>Trabalharemos com pretensão salarial.</li>
                    </ul>


                    <h3>Benefícios:  </h3>
                    <ul>
                        <li>VT, VR, Plano de saúde.</li>
                    </ul>
                    <br>
                    <br>
                    <ul>
                        <li>Os interessados deverão encaminhar o currículo para: recursoshumanos@agenciaopen.com</li>
                    </ul>

                    
                   
                    <h3>Intrépido aventureiro da Infraestrutura, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="991" title="Vaga analista de infraestrutura"]') ; ?>
                               
                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>


<?php get_footer(); ?>
