<?php 

/**
 * The template name: Trabalhe Conosco
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */

require_once ("another-header.php"); ?>

<main>

    <div class="header-trabalhe"></div>
    <div class="content-trabalhe">
        <div class="grid-container">
            <h1><img src="<?php bloginfo('template_url') ?>/build/img/title-trabalhe.png" alt="" /></h1>
            <div class="c-moedas">
                <span class="moeda"></span>
                <span class="moeda"></span>
                <span class="moeda"></span>
            </div>
            <div class="content-txt">
                <p>Há 18 anos nasceu a Agência Open. Essa mocinha dedicada viveu intensamente muitas aventuras e hoje, adulta, tem uma reputação louvável. Durante sua jornada venceu muitos desafios e atendeu clientes de renome como a MRV Engenharia, TOTVS, Banco BMG.</p>
                <p>Agora, como uma verdadeira veterana do marketing digital, se especializou em mais de 23 skills para atender as missões de mais de 300 clientes. Essas proezas foram conquistadas por uma equipe de pelo menos 50 bravos guerreiros e você pode fazer parte dessa party!</p>
            </div>

        </div>
    </div>
    <div class="campo-formulario">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café. </h3>
                <p>Mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-70 tablet-grid-60">
                <h2 class="vaga">Escolha sua <br />classe vaga:</h2>
                <ul class="lista-vagas">
                    <?php
                        // WP_Query arguments
                        $args = array(
                            'post_type'              => array( 'vagas' ),
                        );

                        // The Query
                        $vagas = new WP_Query( $args );

                        // The Loop
                        if ( $vagas->have_posts() ) {
                            while ( $vagas->have_posts() ) {
                                $vagas->the_post(); ?>
                                <li>
                                    <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>" id="show-panel"> <?= get_the_title(); ?> </a>
                                    <i class="mais"></i>
                                </li>
                        <?php }
                        }
                        wp_reset_postdata();
                    ?>
                    
                </ul>
            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>


</main>

<?php get_footer(); ?>
