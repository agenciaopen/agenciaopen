<?php 

/**
 * The template name: Interna Trabalhe Vaga CRO
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Analista de CRO</h2>
                    <p>Estamos procurando por você que é fera em otimização, conversão, análise e estratégia. Analista CRO, venha otimizar o nosso time!</p>
                    <h3>Missão:</h3>
                    <ul>
                        <li><span> - </span>Monitorar performance de páginas com foco em conversão</li>
                        <li><span> - </span>Analisar e determinar os requisitos e especificações de conversão</li>
                        <li><span> - </span>Comunicar com clientes para obter orientação para o processo de conversão e definição de objetivos e KPIs</li>
                        <li><span> - </span>Identificar e implementar maneiras de monitorar funis de conversão, otimizando conteúdo, design e fatores de usabilidade</li>
                        <li><span> - </span>Projetar, desenvolver, executar e documentar testes que confirmem o sucesso do processo de conversão</li>
                        <li><span> - </span>Oferecer suporte à equipe técnica de desenvolvimento para realizar testes e melhorias</li>
                        <li><span> - </span>Criar relatórios analíticos e estratégicos que entreguem insights transformadores</li>
                    </ul>
                    <h3>Habilidades necessárias na campanha: </h3>
                    <ul>
                        <li><span> - </span>Dominar práticas de CRO</li>
                        <li><span> - </span>Inglês intermediário</li>
                        <li><span> - </span>Excelente comunicação oral e escrita</li>
                        <li><span> - </span>Experiência com testes A/B</li>
                        <li><span> - </span>Experiência com criação de wireframes</li>
                        <li><span> - </span>Excel / Google Spreadsheets avançado</li>
                        <li><span> - </span>Web Analytics avançado (capacidade de interpretar dados e extrair insights, dar manutenção no rastreamento e identificar gaps na estratégia de capturar dados)</li>
                        <li><span> - </span>Google Analytics avançado</li>
                        <li><span> - </span>Google Data Studio intermediário (construção de dashboards)</li>
                        <li><span> - </span>Google Tag Manager avançado</li>
                        <li><span> - </span>Google Optimize ou similares</li>
                        <li><span> - </span>Mapas de calor (Hotjar, Crazy Egg etc)</li>
                        <li><span> - </span>Noções de Front-end (html, javascript, css)</li>
                        <li><span> - </span>Noções de UX</li>
                    </ul>
                    <h3>Habilidades bônus: </h3>
                    <ul>
                        <li><span> - </span>Certificados na área de CRO</li>
                        <li><span> - </span>Certificação Google Analytics Qualified Individual</li>
                        <li><span> - </span>Certificação Google Tag Manager Fundamentals - Google Academy</li>
                    </ul>
                    <h3>Atributos comportamentais: </h3>
                    <ul>
                        <li><span> - </span>Autogestão e organização são fundamentais </li>
                        <li><span> - </span>Olhar aguçado para detalhes</li>
                        <li><span> - </span>Ter muita curiosidade e pensamento crítico </li>
                        <li><span> - </span>Capacidade analítica e afinidade com números e métricas</li>
                        <li><span> - </span>Facilidade de aprendizado</li>
                        <li><span> - </span>Capacidade de delegar tarefas para outros setores</li>
                    </ul>
                    <h3>Duração da jornada: </h3>
                    <p>08 horas diárias - Segunda à Sexta-feira</p>
                    <h3>Intrépido aventureiro do CRO, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="911" title="Vaga analista CRO"]') ; ?>
                               
                                <!-- <div class="campo campo-input ">
                                    <input class="" type="text" placeholder="Nome" />
                                </div>
                                <div class="campo campo-input ">
                                    <input class="" type="email" placeholder="E-mail" />
                                </div>
                                <div class="campo-m campo-input ">
                                    <input class="cel" type="text" placeholder="Telefone" />
                                </div>
                                <div class="campo-m campo-input ">
                                   <div id="upload-file">
                                       <input id='input-file' type='file' value='' />
                                       <span>Anexar Currículo</span>
                                    </div>
                                                                            
                                      
                                </div>
                                <div class="campo">
                                    <div class="campo-input">
                                        <i class="icon-seta-btn-01"></i>
                                        <input type="submit" value="Iniciar Aventura" class="btn" />
                                    </div>
                                </div> -->

                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>