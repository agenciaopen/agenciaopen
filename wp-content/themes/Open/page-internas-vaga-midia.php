<?php 

/**
 * The template name: Interna Trabalhe Vaga Mídia
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Analista de Mídia</h2>
                    <p>Você curte novos desafios e busca alcançar resultados ainda melhores em sua carreira? Então, suas metas dão match com nossos KPIs. Não perca tempo e venha se engajar com nossa equipe!</p>
                    <h3>Missão:</h3>
                    <ul>
                        <li><span> - </span>Análise de dados e estratégias digitais</li>
                        <li><span> - </span>Planejamento de mídia online/digital</li>
                        <li><span> - </span>Criação de campanhas nas plataformas Google AdWords, Facebook / Instagram, Linkedin Ads e outras</li>
                        <li><span> - </span>Controlar a performance das campanhas de acordo com as metas de ROI</li>
                        <li><span> - </span>Monitoramento de métricas e KPIs no Google Analytics</li>
                        <li><span> - </span>Controlar orçamento e investimento em mídia</li>
                        <li><span> - </span>Relatórios e apresentações mensais de desempenho</li>
                        <li><span> - </span>Integração de ferramentas e pixels para rastreamento de conversões</li>
                        <li><span> - </span>Análise de mercado e oportunidades</li>
                        <li><span> - </span>Análise dos conteúdos de pré lançamento do site dos clientes (SEO)</li>
                    </ul>
                    <h3>Habilidades necessárias na campanha: </h3>
                    <ul>
                        <li><span> - </span>Capacidade analítica e afinidade com números e métricas</li>
                        <li><span> - </span>Experiência com Google Tag Manager e Google Analytics</li>
                        <li><span> - </span>Domínio do pacote Office (principalmente planilhas)</li>
                        <li><span> - </span>Conhecimento de HTML voltado para SEO</li>
                        <li><span> - </span>Análise SEO On Page</li>
                    </ul>
                    <h3>Habilidades bônus: </h3>
                    <ul>
                        <li><span> - </span>Experiência anterior em Agência Digital</li>
                        <li><span> - </span>Conhecimentos em automações no Zapier</li>
                        <li><span> - </span>Análise CRO (usabilidade e testes A/B)</li>
                        <li><span> - </span>Certificações do Google</li>
                        <li><span> - </span>Certificação de Marketing de Conteúdo - Rock Content e/ou Hubspot</li>
                    </ul>
                    <h3>Atributos comportamentais: </h3>
                    <ul>
                        <li><span> - </span>Autogestão </li>
                        <li><span> - </span>Capacidade analítica e afinidade com números e métricas</li>
                        <li><span> - </span>Boa comunicação oral e escrita </li>
                        <li><span> - </span>Manter-se atualizado com as últimas tendências de marketing digital</li>
                        <li><span> - </span>Facilidade de aprendizado </li>
                        <li><span> - </span>Capacidade de delegar tarefas para outros setores</li>

                    </ul>
                    <h3>Duração da jornada: </h3>
                    <p>08 horas diárias - Segunda à Sexta-feira</p>
                    <h3>Intrépido aventureiro de mídia, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="912" title="Vaga analista Mídia"]') ; ?>
                               
                                <!-- <div class="campo campo-input ">
                                    <input class="" type="text" placeholder="Nome" />
                                </div>
                                <div class="campo campo-input ">
                                    <input class="" type="email" placeholder="E-mail" />
                                </div>
                                <div class="campo-m campo-input ">
                                    <input class="cel" type="text" placeholder="Telefone" />
                                </div>
                                <div class="campo-m campo-input ">
                                   <div id="upload-file">
                                       <input id='input-file' type='file' value='' />
                                       <span>Anexar Currículo</span>
                                    </div>
                                                                            
                                      
                                </div>
                                <div class="campo">
                                    <div class="campo-input">
                                        <i class="icon-seta-btn-01"></i>
                                        <input type="submit" value="Iniciar Aventura" class="btn" />
                                    </div>
                                </div> -->

                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>
