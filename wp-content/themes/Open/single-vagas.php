<?php 
    require_once ("another-header.php"); 
?>

<main>
    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>

            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    
                <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post(); ?> 
                        <h2><?= get_the_title(); ?></h2>

                        <?php the_content(); ?>

                        <div class="grid-70 suffix-30">
                            <div class="form-curriculo form">
                                <?php echo do_shortcode('[contact-form-7 id="911" title="Vaga analista CRO"]') ; ?>
                            </div>
                        </div>
                <?php   endwhile;
                        wp_reset_postdata(); 
                    endif;
                ?>    
                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>