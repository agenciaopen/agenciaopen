<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>
        <?php wp_title(''); ?> </title>
    <meta name="description" content=" " />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <!--[if (lt IE 9) & (!IEMobile)]>
    <script src="build/js/modernizr-custom.js"></script>
    <link rel="stylesheet" href="build/css/ie.css" />
    <![endif]-->
    <!--[if (lt IE 8) & (!IEMobile)]>
    <![endif]-->
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KSWXBK');</script>
<!-- End Google Tag Manager -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/build/css/libs.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/build/css/style.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class( 'interna-blog page-template-page-tabalhe-php ' ); ?>>

    <header class="header" id="header-site">
        <div class="header-top">
            <div class="grid-25 tablet-grid-50 mobile-grid-80">
                <?php if (is_page( 'blog' ) || (is_category()) || (is_single())) {?>
                <a href="<?php bloginfo('siteurl') ?>/blog/" title="" class="logo">
                  
                    <img src="<?php bloginfo('template_url') ?>/build/img/logo-blog.png" />
                       </a>
                <?php } else { ?>
                <a href="<?php bloginfo('siteurl') ?>/" title="" class="logo">
                    <img src="<?php bloginfo('template_url') ?>/build/img/logo-open.png" />
                </a>
                <?php } ?>

            </div>
            <div class="grid-10 prefix-65 tablet-grid-15 tablet-prefix-35 mobile-grid-20">
                <div class="menu-section">
                    <div class="menu-toggle">
                        <div class="one"></div>
                        <div class="two"></div>
                        <div class="three"></div>
                    </div>

                    <nav class="busca">
                        <div role="navigation" class="hidden hover">

                            <ul id="menu-header">
                                <li><a href="<?php bloginfo('siteurl') ?>/">Home</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/#clientes">clientes</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/#objetivos">serviços</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/#depoimentos">depoimentos</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/#contato">Contato</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/blog">blog</a></li>
                                <li><a href="<?php bloginfo('siteurl') ?>/trabalhe-conosco">vagas</a></li>
                            </ul>
                            <ul class="social">
                                <li> <a href="https://www.facebook.com/agenciaopen" title="Facebook" target="_blank"><i class="icon-facebook-01"></i></a></li>
                                <li><a href="https://www.instagram.com/agenciaopen_/" title="Instagram" target="_blank"><i class="icon-instagran-01"></i></a></li>
                                
                                <li><a href="https://www.linkedin.com/company/agencia-open-de-internet-e-aplicativos-web-ltda" target="_blank" title="Linkedin"><i class="icon-linkedin-01"></i></a></li>
                            </ul>

                        </div>


                    </nav>
                </div>
            </div>
        </div>


    </header>
