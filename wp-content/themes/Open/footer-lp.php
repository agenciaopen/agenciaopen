<style>
.contato-blog, .aside .pauta, .contato{
	display: none;
}
footer#footer {
    background: #fff;
    border-top: 1px solid #c9c9c9;
}
.header .logo {
    margin: 2px 20px;
}
.header .menu-toggle {
    display: none;
}
.screen-reader-response, span.wpcf7-not-valid-tip {
    display: none !important;
}
.pg404{
	background: transparent;
}
.page .header, .page-template-page-blog .header, .single-post .header, .category .header, #header-site {
    background: #fff;
    box-shadow: 0 8px 6px -6px rgba(0,0,0,.73);
}
@media screen and (max-width: 767px){
.header {
    height: auto;
    text-align: center;
}
	.header-top .grid-25.tablet-grid-50.mobile-grid-80 {
    width: 100% !important;
}

}

</style>
    <?php wp_footer();?>
  
</body>
  </html>

