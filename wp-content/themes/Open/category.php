<?php 

require_once("another-header.php"); ?>
<?php require_once('menu-categorias.php'); ?>
<section class="content-blog">
    <div class="grid-container grid-parent">
        <div class="grid-70 grid-parent">
            <?php
	

    $cat = get_query_var('cat');
    $category = get_category ($cat);
    $cont = 0;

    // WP_Query arguments
    // $args = array (
    //     'posts_per_page'  => 1,
    //     'cat'        => $cat      
    // );

    // // The Query
    // $loop = new WP_Query( $args );
    
    // The Loop
    if ( have_posts() ) {
        while ( have_posts() ) : the_post(); 

            //get category infos
            $categoria     = get_the_category($post->id);
            $idCategoria   = $categoria[0]->term_id;
            $nomeCategoria = $categoria[0]->cat_name;
            $linkCategoria = get_category_link( $idCategoria );
           
            ?>

                <div class="grid-50 tablet-grid-50 tablet-grid-parent ">
                    <div class="box-blog <?php $the_post_cats = dw_get_category(); echo $the_post_cats['slug'];?>">
                        <figure>
                           <img title="" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')" />
                                <figcaption>
                                    <div class="description">

                                        <h2 class="title">
                                            <?php the_title(); ?>
                                        </h2>
                                        <hr class="line-post" />
                                        <p>
                                            <?php wp_limit_post(150,'...',true);?>
                                        </p>
                                    </div>

                                    <span class="date"><?php echo get_the_date(); ?></span>
                                    <h2 class="title">
                                        <?php the_title(); ?>
                                    </h2>
                                      <div class="bx"></div>
                                    <hr class="line-post" />
                                    <div class="grid-100 tablet-grid-100  mobile-grid-100">
                                        <a href="<?php the_permalink(); ?>" title="" class="btn btn-blog btn-depoimentos btn-blog-c">
                                        Continuar lendo
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                                    </div>
                                </figcaption>
                        </figure>

                    </div>


                </div>

  <?php endwhile; 
                    
    } else { ?>
				
          <?php } 

        wp_reset_postdata(); 
    ?>
        </div>
        <div class="grid-25 grid-parent prefix-5 tablet-grid-60 tablet-prefix-20 tablet-suffix-20 mobile-grid-90 mobile-prefix-5 mobile-suffix-5">
            <div class="aside">
                <?php get_sidebar();  ?>
            </div>

        </div>
          
    </div>

    


</section>
<?php require_once('footer-blog.php'); ?>
