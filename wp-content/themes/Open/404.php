

<?php include_once 'header.php';  ?>
<main>

    <section class="pg404 interno">

        <div class="grid-container" style="min-height: 100vh; align-items: center; display: flex;">
            <div class="grid-70 prefix-15 suffix-15 tablet-grid-80 tablet-prefix-10 tablet-suffix-10">
                <div class="grid-100 tablet-grid-100">
                



                    <h1 style="color: #000;">A página que você procura não foi encontrada.  </h1>
                    <p style="color: #ababab;">Mas já que você está aqui, que tal descobrir como podemos te conectar aos seus clientes? <br /> Somos especialistas em estratégias digitais com mais de 18 anos de experiência. Nessa trajetória já atendemos grandes empresas como MRV, Moura Dubeux, Laboratório Lustosa, Kazzas, Banco BMG e TOVTS. </p>
                    <p style="color: #ababab;">Volte a página inicial e conheça mais sobre a Open.  </p>

                    <div class="grid-40 prefix-30 suffix-30 tablet-grid-50 tablet-suffix-25 tablet-prefix-25">
                        <a href="http://www.agenciaopen.com" title="" class="btn btn-clientes">
                            Voltar a página inicial 
                            <i class="icon-seta-btn-01" style="border-left: 2px solid transparent;"></i>
                        </a>
                    </div>
                </div>

            </div>

        </div>

    </section>
</main>
<?php include_once 'footer.php'; ?>

