<?php 

/**
 * The template name: Interna Trabalhe conteudo
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Analista de Conteúdo </h2>
                    
                    <h3>Principais Responsabilidades:</h3>
                    <ul>
                        <li><span> - </span>Criação de conteúdo para estratégias de marketing digital, incluindo mídias sociais, blogs, sites, e-mail marketing e anúncios;</li>
                        <li><span> - </span>Monitoramento de engajamento, interação com usuários e gestão de imagem no ambiente digital;</li>
                        <li><span> - </span>Desenvolvimento de relatórios, analisando os principais indicadores para transformar dados em informações de valor para a conta e para o cliente;</li>
                        <li><span> - </span>Pesquisa e benchmarking constantes para elaboração de linhas editoriais;</li>
                        <li><span> - </span>Revisão e edição de conteúdo.</li>
                    </ul>
                    <h3>Conhecimentos Exigidos: </h3>
                    <ul>
                        <li><span> - </span>Capacidade analítica para tornar o conteúdo mais estratégico com foco em resultados para os clientes; </li>
                        <li><span> - </span>Experiência nas técnicas de Marketing de Conteúdo e Inbound Marketing; </li>
                        <li><span> - </span>Excelente texto, com fluidez para transitar de conteúdos voltados para B2B e B2C.</li>
                        <li><span> - </span>Criatividade, bom humor, proatividade, boa comunicação pessoal e gosto pelo trabalho em equipe. </li>


                    </ul>
                    <h3>Escolaridade:  </h3>
                    <ul>
                        <li>Graduação em Comunicação, Publicidade e Propaganda, Jornalismo ou áreas afins.</li>
                    </ul>

                    <h3>Remuneração:  </h3>
                    <ul>
                        <li>Trabalharemos com pretensão salarial.</li>
                    </ul>


                    <h3>Benefícios:  </h3>
                    <ul>
                        <li>VT, VR, Plano de saúde.</li>
                    </ul>
                    <br>
                    <br>
                    <ul>
                        <li>Os interessados deverão encaminhar o currículo para: recursoshumanos@agenciaopen.com</li>
                    </ul>

                    
                   
                    <h3>Intrépido aventureiro do Conteúdo, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="990" title="Vaga analista de conteúdo"]') ; ?>
                               
                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>
