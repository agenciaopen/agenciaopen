<?php 

/**
 * The template name: Interna Trabalhe negocios
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Consultor de Novos Negócios  </h2>
                    
                    <h3>Principais Responsabilidades:</h3>
                    <ul>
                        <li><span> - </span>Profissional experiente em atendimento e prospecção de clientes para o marketing digital e representação comercial atuando e identificando oportunidades de ampliar negócios;</li>
                        <li><span> - </span>Prospectar e realizar visitas externas às empresas para fazer apresentações dos serviços de comunicação digital oferecidos pela Agência Open, a fim de negociar novos contratos e renovações;</li>
                        <li><span> - </span>Irá fechar contratos de campanhas de marketing digital com empresas e projetos de sites, portais online e sistemas web;</li>
                         <li><span> - </span>Agendamento de reuniões de apresentação da agência e os serviços oferecidos e fechamento de contratos;</li>
                        <li><span> - </span>Habilidades em vendas ativas, prospecção de clientes, negociação e fechamento de venda de serviços e projetos web;</li>
                        <li><span> - </span>Conhecimento em vendas de serviços de comunicação online e mídia e conhecimentos em tecnologia;</li>
                        <li><span> - </span>Perfil de fácil convívio, comunicativo e persuasivo, boa dicção e escrita.</li>
                      
                    </ul>
                    <h3>Conhecimentos Exigidos: </h3>
                    <ul>
                        <li><span> - </span>Conhecimento em marketing digital na geração de lead;</li>
                        <li><span> - </span>Conhecimento em vendas e negociação de Fee;</li>
                        <li><span> - </span>Conhecimento em projetos web tais como sites e portais;</li>
                        <li><span> - </span>Ter vivência em prospecção de clientes;</li>
                        <li><span> - </span>Requisitos / Job requirements;</li>
                        <li><span> - </span>Será um diferencial o candidato que tiver familiaridade com Google, Facebook e Instagram Ads;</li>
                        <li><span> - </span>Fluência em inglês será um diferencial;</li>
                        <li><span> - </span>Disponibilidade para viagens curtas.</li>
                    </ul>
                    <h3>Escolaridade:  </h3>
                    <ul>
                        <li>Graduação em Marketing, Publicidade ou áreas afins.</li>
                    </ul>

                    <h3>Remuneração:  </h3>
                    <ul>
                        <li>Trabalharemos com pretensão salarial.</li>
                    </ul>


                    <h3>Benefícios:  </h3>
                    <ul>
                        <li>VT, VR, Plano de saúde.</li>
                    </ul>
                    <br>
                    <br>
                    <ul>
                        <li>Os interessados deverão encaminhar o currículo para: recursoshumanos@agenciaopen.com</li>
                    </ul>

                    
                   
                    <h3>Intrépido aventureiro dos novos negócios, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="992" title="Vaga consultor de novos negócios"]') ; ?>
                               
                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>
