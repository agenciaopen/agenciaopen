<?php get_header(); ?>
<main class="main">
    <section class="home" id="home">
        <div id="hero-slider">
            <div class="hero-slide" style="background-image: url('<?php bloginfo('template_url') ?>/build/img/slider-home.jpg');background-position: center top;    background-size: cover;">
                <div class="hero-content text-center">
                    <div class="grid-90 prefix-5 suffix-5 tablet-grid-90 tablet-prefix-5 tablet-suffix-5 mobile-grid-100">
                        <div class="grid-33 prefix-5 tablet-grid-60">

                            <div class="box box-home" data-animation="fadeInLeftBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>
                                <p class="txt3">+ D 15 anos atuando como agência digital da MRV Engenharia</p>
                            </div>
                            <div class="grid-70 prefix-30 grid-parent tablet-grid-70 tablet-prefix-30">
                                <a href="#servicos" title="" class="btn btn-clientes scrollLink" data-animation="fadeInUpBig" data-delay="0.5s">
                                Conheca nossos serviços
                                <i class="icon-seta-btn-01"></i>
                            </a>
                            </div>


                        </div>
                    </div>


                    <!--  <div class="grid-33">
                             <div class="box box-home" data-animation="fadeInUpBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                            </div>
                        </div>

                        <div class="grid-33">
                                <div class="box box-home" data-animation="fadeInRightBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                            </div>

                        </div> -->
                </div>
            </div>

            <div class="hero-slide" style="background-image: url('<?php bloginfo('template_url') ?>/build/img/slider-home2.jpg');background-position: center top;">
                <div class="hero-content text-center">
                    <div class="grid-90 prefix-5 suffix-5 tablet-grid-90 tablet-prefix-5 tablet-suffix-5 mobile-grid-100">
                        <div class="grid-33 prefix-33 suffix-33 tablet-grid-60">

                            <div class="box box-home" data-animation="fadeInUp" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>
                                <p class="txt3">+ D 300 marcas se abriram para o digital com a Agência Open</p>
                            </div>
                            <div class="grid-70 prefix-15 auffix-15 grid-parent tablet-grid-70 tablet-prefix-30">
                                <a href="#servicos" title="" class="btn btn-clientes scrollLink" data-animation="fadeInUpBig" data-delay="0.5s">
                                Conheca nossos serviços
                                <i class="icon-seta-btn-01"></i>
                            </a>
                            </div>


                        </div>
                    </div>


                </div>
            </div>

            <div class="hero-slide" style="background-image: url('<?php bloginfo('template_url') ?>/build/img/slider-home3.jpg');background-position: center top; background-size: cover;">
                <div class="hero-content text-center">
                    <div class="grid-90 prefix-5 suffix-5 tablet-grid-90 tablet-prefix-5 tablet-suffix-5 mobile-grid-100">
                        <div class="grid-33 prefix-66 tablet-grid-60">

                            <div class="box box-home" data-animation="fadeInRightBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>
                                <p class="txt3">+ D 60 especialistas trabalhando para o sucesso da sua marca</p>
                            </div>
                            <div class="grid-70 suffix-30 grid-parent tablet-grid-70 tablet-prefix-30">
                                <a href="#servicos" title="" class="btn btn-clientes scrollLink" data-animation="fadeInUpBig" data-delay="0.5s">
                                Conheca nossos serviços
                                <i class="icon-seta-btn-01"></i>
                            </a>
                            </div>


                        </div>
                    </div>


                    <!--  <div class="grid-33">
                             <div class="box box-home" data-animation="fadeInUpBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                            </div>
                        </div>

                        <div class="grid-33">
                                <div class="box box-home" data-animation="fadeInRightBig" data-delay="0.5s">
                                <div class="left-border left"></div>
                                <div class="top-border right"></div>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                            </div>

                        </div> -->
                </div>
            </div>


        </div>





        <div class="content-mouse hide-on-mobile hide-on-tablet">
            <div class="scroll-down svg" id="home-scroll-down">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_2" x="0px" y="0px" viewBox="0 0 25.166666 37.8704414" enable-background="new 0 0 25.166666 37.8704414" xml:space="preserve">
                        <path class="stroke" fill="none" stroke="#c7c4b8" stroke-width="2.5" stroke-miterlimit="10" d="M12.5833445 36.6204414h-0.0000229C6.3499947 36.6204414 1.25 31.5204487 1.25 25.2871208V12.5833216C1.25 6.3499947 6.3499951 1.25 12.5833216 1.25h0.0000229c6.2333269 0 11.3333216 5.0999947 11.3333216 11.3333216v12.7037992C23.916666 31.5204487 18.8166714 36.6204414 12.5833445 36.6204414z"></path>
                        <path class="scroller" fill="#c7c4b8" d="M13.0833359 19.2157116h-0.9192753c-1.0999985 0-1.9999971-0.8999996-1.9999971-1.9999981v-5.428606c0-1.0999994 0.8999987-1.9999981 1.9999971-1.9999981h0.9192753c1.0999985 0 1.9999981 0.8999987 1.9999981 1.9999981v5.428606C15.083334 18.315712 14.1833344 19.2157116 13.0833359 19.2157116z"></path>
                    </svg><i class="icon icon-arrow-down"></i>
            </div>
        </div>
        <div class="q-home">
            <div class="q1 left1"></div>
            <div class="q1 right1"></div>
        </div>

    </section>
    <section class="clientes" id="clientes">
        <div class="q-3"></div>
        <div class="q-4"></div>

        <div class="title">

            <h2>Clientes Atendidos</h2>
            <span>Conheça as marcas que abriram sua mente para o digital com a Open</span>
            <hr />
        </div>
        <div class="logo-clientes cf">
            <div class="grid-100 ">
                <div class="grid-70 prefix-15 suffix-15 ">
                    <div id="c-logos">
                        <div>
                            <div class="grid-33 grid-parent tablet-grid-33 mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/MRV.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">

                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/BancoInter.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/BMG.png" alt="" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/AYoshii.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Metrocasa.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/ACMinas.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Emccamp.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Bernoulli.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Minasflor.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="grid-33 grid-parent tablet-grid-33 mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Mito.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Atex.png" alt="" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Geraldo-Lustosa.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Caparao.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Itau.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/TOTVS.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Lockey.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Datamyne.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Realiza.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="grid-33 grid-parent tablet-grid-33 mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Cemig.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Captalys.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Multimed.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Elmo.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Accor.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Divina.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Bom-Sucesso.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Goodlife.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 grid-parent tablet-grid-33  mobile-grid-50">
                                <div class="content-lista cf">
                                    <div class="table">
                                        <div class="vertical">
                                            <img src="<?php bloginfo('template_url') ?>/build/img/logos/Fina_forma.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </div>


    </section>
    <section class="objetivos" id="servicos">
        <div class="table">
            <div class="vertical">
                <div class="grid-25 grid-parent fullHeight tablet-grid-33 mobile-grid-100">
                    <div class="bx-vermelho">
                        <div class="title">
                            <h2>Seus objetivos:</h2>
                            <hr />
                            <span class="hide-on-mobile">Selecione ao <strong>lado </strong>seus objetivos e veja <strong>abaixo</strong>  quais serviços podem ajudá-lo</span>

                            <span class="hide-on-desktop hide-on-tablet">Selecione <strong>abaixo </strong>seus objetivos e veja<strong>, logo após, </strong>  quais serviços podem ajudá-lo</span>

                        </div>
                    </div>
                    <div class="bx-img hide-on-mobile cf">
                        <div class="content-list scrollbar-inner" id="scrollbar-inner">

                            <ul class="lista"  id="lista-ob">
                                <li class="bx1">Landing Pages</li>
                                <li class="bx1">Campanhas online</li>
                                <li class="bx1">Otimização de site</li>
                                <li class="bx1">Consultoria de planejamento</li>
                                <li class="bx2">Produção de conteúdo em blog</li>
                                <li class="bx2">Relacionamento em redes sociais</li>
                                <li class="bx2">Higienização da marca no cenário online</li>
                                <li class="bx3">Otimização para os mecanismos de pesquisa</li>
                                <li class="bx3">Campanha no Google Adwords</li>
                                <li class="bx3">Melhoria e produção de conteúdo</li>
                                <li class="bx4">Consultoria estratégica</li>
                                <li class="bx4">Diagnóstico de oportunidades</li>
                                <li class="bx4">Benchmarking</li>
                                <li class="bx4">Pesquisa de mercado</li>
                                <li class="bx5">Planejamento de mídia online</li>
                                <li class="bx5">Gestão de investimento</li>
                                <li class="bx5">Criação e otimização de campanhas online</li>
                                <li class="bx6">Desenvolvimento de site ou sistema</li>
                                <li class="bx6">Melhoria da experiência do usuário</li>
                                <li class="bx7">Criação de landing page</li>
                                <li class="bx7">Criação de campanhas online</li>
                                <li class="bx7">Automação de marketing</li>
                                <li class="bx7">E-mail marketing</li>
                                <li class="bx8">Otimização dos canais de entrada de leads</li>
                                <li class="bx8">Análise e otimização de campanhas online</li>
                                <li class="bx8">Testes AB</li>
                                <li class="bx9">Criação de personas</li>
                                <li class="bx9">Produção de conteúdo/materiais ricos</li>
                                <li class="bx9">Otimização de conteúdo para rankeamento</li>



                            </ul>

                        </div>
                        <div class="grid-80 prefix-10 suffix-10">
                            <a href="#contato" title="" class="btn btn-clientes  " id="btn-obj">
                            Quero uma consultoria
                            <i class="icon-seta-btn-01"></i>
                        </a>
                        </div>

                    </div>
                </div>
                <div class="grid-75 grid-parent tablet-grid-66">
                    <div class="grid-90 prefix-5 suffix-5">
                        <div class="box-objetivo">
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="1">
                                    <h3>Aumentar as <br /> minhas vendas</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="2">
                                    <h3>Melhorar a reputação <br /> da minha marca</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="3">
                                    <h3>Ver meu site na <br /> 1ª página do Google</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="4">
                                    <h3>Identificar fraquezas <br /> e oportunidades</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="5">
                                    <h3>Investir corretamente <br /> nas campanhas online</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="6">
                                    <h3>Criar ou <br />reformular o meu site</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="7">
                                    <h3>Iniciar captação <br /> de leads</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="8">
                                    <h3>Melhorar captação <br /> de leads</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="grid-33 tablet-grid-33">
                                <div class="content-box-objetivo cf" id="9">
                                    <h3>Produzir conteúdo <br />relevante</h3>
                                    <hr />
                                    <div class="grid-80 prefix-10 suffix-10 acoes">
                                        <i class="icon-v-01  ativar"></i>
                                        <i class="icon-x-01  desativar"></i>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="grid-100 grid-parent mobile-grid-100 cf">


                </div>
            </div>
        </div>
        <div class="bx-img hide-on-desktop hide-on-tablet">
            <p>Veja nossos serviços e soluções para os objetivos que você marcou:</p>
            <div class="content-list scrollbar-inner" id="scrollbar-inner">

                <ul class="lista" id="lista-ob-m">
                    <li class="bx1">Landing Pages</li>
                    <li class="bx1">Campanhas online</li>
                    <li class="bx1">Otimização de site</li>
                    <li class="bx1">Consultoria de planejamento</li>
                    <li class="bx2">Produção de conteúdo em blog</li>
                    <li class="bx2">Relacionamento em redes sociais</li>
                    <li class="bx2">Higienização da marca no cenário online</li>
                    <li class="bx3">Otimização para os mecanismos de pesquisa</li>
                    <li class="bx3">Campanha no Google Adwords</li>
                    <li class="bx3">Melhoria e produção de conteúdo</li>
                    <li class="bx4">Consultoria estratégica</li>
                    <li class="bx4">Diagnóstico de oportunidades</li>
                    <li class="bx4">Benchmarking</li>
                    <li class="bx4">Pesquisa de mercado</li>
                    <li class="bx5">Planejamento de mídia online</li>
                    <li class="bx5">Gestão de investimento</li>
                    <li class="bx5">Criação e otimização de campanhas online</li>
                    <li class="bx6">Desenvolvimento de site ou sistema</li>
                    <li class="bx6">Melhoria da experiência do usuário</li>
                    <li class="bx7">Criação de landing page</li>
                    <li class="bx7">Criação de campanhas online</li>
                    <li class="bx7">Automação de marketing</li>
                    <li class="bx7">E-mail marketing</li>
                    <li class="bx8">Otimização dos canais de entrada de leads</li>
                    <li class="bx8">Análise e otimização de campanhas online</li>
                    <li class="bx8">Testes AB</li>
                    <li class="bx9">Criação de personas</li>
                    <li class="bx9">Produção de conteúdo/materiais ricos</li>
                    <li class="bx9">Otimização de conteúdo para rankeamento</li>



                </ul>

            </div>
            <div class="grid-80 prefix-10 suffix-10">
                <a href="#contato" title="" class="btn btn-clientes " id="btn-obj">
                            Quero uma consultoria
                            <i class="icon-seta-btn-01"></i>
                        </a>
            </div>

        </div>
    </section>
    <section class="depoimentos" id="depoimentos">
        <div class="title">
            <h2>palavras de quem conhece</h2>
            <span>Leia os depoimentos dos clientes que se abriram para falar de uma parceria de sucesso:</span>
            <hr />
        </div>
        <div class="grid-100 grid-parent">
            <div class="slider-depoimentos" id="slider-depoimentos">
                
                   <div>
                    <div class="grid-45 prefix-10 grid-parent tablet-grid-45 tablet-prefix-5">
                        <img src="<?php bloginfo('template_url') ?>/build/img/depoimento-open.jpg" alt="" />
                    </div>
                    <div class="grid-30 prefix-5 suffix-10 grid-parent tablet-grid-45 tablet-prefix-5">
                        <div class="content-depoimento">
                            <span class="">“</span>
                            <p>A Open é parceira MRV em projetos digitais há mais de 15 anos. Consolidamos uma parceria sólida e duradoura graças ao constante alinhamento, aos bons resultados conquistados e, claro, ao relacionamento e respeito mútuos. </p>
                            <p class="autor">Fabiana Afonso -  Coordenadora de Marketing <br /> <strong>MRV Engenharia</strong></p>
                            <!-- <div class="grid-70 prefix-15 suffix-15 tablet-grid-80 tablet-prefix-10 tablet-suffix-10">
                                <a href="" title="" class="btn btn-depoimentos">
                                        Conheca este case
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                            </div> -->
                        </div>
                    </div>
                </div>
                
                <div>
                    <div class="grid-45 prefix-10 grid-parent tablet-grid-45 tablet-prefix-5">
                        <img src="<?php bloginfo('template_url') ?>/build/img/img-slide.jpg" alt="" />
                    </div>
                    <div class="grid-30 prefix-5 suffix-10 grid-parent tablet-grid-45 tablet-prefix-5">
                        <div class="content-depoimento">
                            <span class="">“</span>
                            <p>Em parceria com a Open, conseguimos levar a Atex do nível de usuário em redes sociais para o nível de profissional em publicações e gestão do marketing digital. E o melhor é que o retorno se dá não só no campo do reforço da imagem da empresa, mas também no financeiro. É um investimento que se paga! Recomendo! </p>
                            <p class="autor">Cristiano Drumond - Gerente Geral - <strong>Atex</strong></p>
                            <!-- <div class="grid-70 prefix-15 suffix-15 tablet-grid-80 tablet-prefix-10 tablet-suffix-10">
                                <a href="" title="" class="btn btn-depoimentos">
                                        Conheca este case
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                            </div> -->
                        </div>
                    </div>
                </div>
                
              
                

            </div>
        </div>


    </section>
    <div class="blog-home" id="blog">
        <div class="grid-25 grid-parent fullHeight tablet-grid-50 tablet-grid-parent">
            <div class="box-blog">
                <div class="mask"></div>
                <div class="chamada-blog">
                    <div class="table">
                        <div class="vertical">
                            <h2>Blog da open</h2>
                            <hr />
                            <p>Coloque seus óculos de leitura e fique por dentro das novidades do mundo do marketing digital. </p>
                            <div class="grid-100 tablet-grid-100">
                                <a href="<?php bloginfo('siteurl') ?>/blog" title="Blog" class="btn btn-blog btn-depoimentos">
                                        Ir para o Blog
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <?php query_posts("posts_per_page=3"); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="grid-25 grid-parent fullHeight tablet-grid-50 tablet-grid-parent lista">



            <figure>
                <?php
                          // if ( has_post_thumbnail() ) {
                           //     the_post_thumbnail( 'miniatura' );
                          //  } 
                
                	 $thumb_id = get_post_thumbnail_id();
                // A URL da imagem
                if ( $thumb_id )
                    $thumb_url = wp_get_attachment_url( $post_thumbnail_id );
                
                ?>
                    <img title="" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')" />
                    <figcaption>
                        <div class="description">
                            <h2 class="title">
                                <?php the_title(); ?>
                            </h2>
                        </div>
                        <h2 class="title">
                            <?php the_title(); ?>
                        </h2>
                        <div class="grid-100 tablet-grid-100  mobile-grid-100">
                            <a href="<?php the_permalink(); ?>" title="" class="btn btn-blog btn-depoimentos btn-blog-c">
                                        Ir para o Blog
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                        </div>
                    </figcaption>
            </figure>



        </div>
        <?php endwhile; ?>
        <?php endif; ?>




    </div>
    <?php get_footer(); ?>
