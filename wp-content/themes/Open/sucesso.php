<?php 


/**
 * The template name: Sucesso
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


include_once 'header.php';  ?>
<main>

    <section class="sucesso ">
        <div class="table">
            <div class="vertical">

                <div class="grid-container">

                    <div class="grid-50 prefix-25 suffix-25">
                        <div class="grid-100 tablet-grid-50">

                            <h1>Mensagem enviada <br />com sucesso!</h1>
                            <hr />
                            <p>Tome um cafezinho e aguarde, em breve a gente se fala!</p>
                            <div class="grid-50 tablet-grid-60 grid-parent">
                                <a href="<?php bloginfo('siteurl') ?>/" title="" class="btn btn-clientes">
                            Ir para home
                            <i class="icon-seta-btn-01"></i>
                        </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>
</main>
<?php include_once 'footer.php'; ?>
