<?php 

/**
 * The template name: Blog
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once("another-header.php"); ?>
<?php require_once('menu-categorias.php'); ?>
<section class="content-blog">
    <div class="grid-container grid-parent">
        <div class="grid-70 grid-parent">
            <?php query_posts("posts_per_page=10"); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


            <div class="grid-50 tablet-grid-50 tablet-grid-parent ">
                <div class="box-blog <?php $the_post_cats = dw_get_category(); echo $the_post_cats['slug'];?>">
                    <figure>
                        <?php
                          // if ( has_post_thumbnail() ) {
                           //     the_post_thumbnail( 'miniatura' );
                          //  } 
                
                	 $thumb_id = get_post_thumbnail_id();
                // A URL da imagem
                if ( $thumb_id )
                    $thumb_url = wp_get_attachment_url( $post_thumbnail_id );
                
                ?>
                            <img title="" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')" />
                            <figcaption>
                                <div class="description">

                                    <h2 class="title">
                                        <?php the_title(); ?>
                                    </h2>
                                    <hr class="line-post" />
                                    <p>
                                        <?php wp_limit_post(150,'...',true);?>
                                    </p>
                                </div>

                                <span class="date"><?php echo get_the_date(); ?></span>
                                <h2 class="title">
                                   
                                    <?php the_title(); ?>
                                </h2>
                                 <div class="bx"></div>
                                <hr class="line-post" />
                                <div class="grid-100 tablet-grid-100  mobile-grid-100">
                                    <a href="<?php the_permalink(); ?>" title="" class="btn btn-blog btn-depoimentos btn-blog-c">
                                        Continuar lendo
                                        <i class="icon-seta-btn-01"></i>
                                    </a>
                                </div>
                                
                            </figcaption>
                    </figure>

                </div>


            </div>



            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="grid-30 tablet-grid-80 tablet-prefix-10 tablet-suffix-10 mobile-grid-90 mobile-prefix-5 mobile-suffix-5">
            <div class="aside">
                <?php get_sidebar();  ?>
            </div>

        </div>
    </div>




</section>
<?php require_once('footer-blog.php'); ?>
