var path = require('path');
var filenames = require("gulp-tap");
var gulp = require('gulp');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var browsersync = require('browser-sync').create(),
    browsersyncConfig = {};
var sass = require('gulp-compass');
var minifycss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var plugins;



gulp.task('default', function() {
      gulp.start('dev');
});


function swallowError(err) {
    util.error(err.toString());
    this.emit('end');
}


gulp.task('browser-sync', function() {
    browsersync.init({
      open: 'external',
      port: 3000,
      proxy:"http://localhost:8181/projetos/site-open",
        
        
        
  });
});


gulp.task('sass', function() {
    gulp.src(['./assets/sass/style.scss'])
        .pipe(plumber({
            handleError: swallowError
        }))
        .pipe(sass({
            project: __dirname,
            css: './build/css',
            sourcemap: true,
            sass: './assets/sass',
            style: 'compressed',
            task: 'watch'
        }));
});


gulp.task('updateCSS', function() {
    gulp.src(['./build/css/*.css'])
        .pipe(browsersync.stream({ match: '**/*.css' }));
})



gulp.task('uglifyAssetsJS', function() {
    gulp.src('./assets/js/*.js')
        .pipe(plumber({
            handleError: swallowError
        }))
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'))
        .pipe(browsersync.stream({ match: '**/*.js' }));
});




gulp.task('uglifyLibsJS', function() {
    plugins = require('./plugins.json');

    for (var i = 0; plugins['js'].length > i; i++) {
        plugins['js'][i] = path.join('./lib/', plugins['js'][i]);
    }
});

gulp.task('libsJS', ['uglifyLibsJS'], function() {
    gulp.src(plugins['js'])
        .pipe(plumber({
            handleError: swallowError
        }))
        .pipe(uglify())
        .pipe(concat('libs.min.js')) 
        .pipe(gulp.dest('./build/js'))
        .pipe(browsersync.stream({ match: '**/*.js' }));
    
});



gulp.task('joinPathsLibsCSS', function() {
    plugins = require('./plugins.json');

    for (var i = 0; plugins['css'].length > i; i++) {
        plugins['css'][i] = path.join('lib/', plugins['css'][i]);
    }
});

gulp.task('libsCSS', ['joinPathsLibsCSS'], function() {
    gulp.src(plugins['css'])
        .pipe(plumber({
            handleError: swallowError
        }))
        .pipe(concat('libs.min.css'))
        .pipe(gulp.dest('./build/css'))
        .pipe(minifycss())
        .pipe(gulp.dest('./build/css'))
        .pipe(browsersync.stream({ match: '**/*.css' }));
});





gulp.task('watch', function() {
    watch("./assets/js/**/*.js", function() {
        gulp.start('uglifyAssetsJS');
    });

   watch("./plugins.json", function() {
        gulp.start('libsJS');
        gulp.start('libsCSS');
    });

    watch("./build/css/**/*.css", function() {
        gulp.start('updateCSS');
    });



});


gulp.task('dev', ['watch', 'sass', 'browser-sync' ]);
