<?php 

/**
 * The template name: Interna Trabalhe
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>


    <div class="campo-formulario campo-formulario-interno cf">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-50 suffix-20 tablet-grid-60">
                <div id="interna-vagas">
                    <h2>Analista de SEO</h2>
                    <p>O mundo digital precisa ser salvo dos maus monitoramentos. Se você é um corajoso analista de SEO que sabe realmente como aumentar em<span> mais de  8mil </span> XXXX% o tráfego orgânico de um site, blog etc. tudo isso por meio de monitoramentos, análises e otimização dos resultados, é de você que nossa equipe precisa!</p>
                    <h3>Missão:</h3>
                    <ul>
                        <li><span> - </span>Monitoramento, análise e definição de ações de SEO on e off-page</li>
                        <li><span> - </span>Suporte à equipe técnica de desenvolvimento para realizar melhorias</li>
                        <li><span> - </span>Suporte à equipe de marketing digital para indicar melhorias de conteúdo</li>
                        <li><span> - </span>Criar rastreamentos avançados via Google Tag Manager</li>
                        <li><span> - </span>Criar relatórios analíticos e estratégicos</li>
                    </ul>
                    <h3>Habilidades necessárias na campanha: </h3>
                    <ul>
                        <li><span> - </span>Inglês intermediário </li>
                        <li><span> - </span>Conhecimento avançado sobre rankeamento </li>
                        <li><span> - </span>Google Analytics </li>
                        <li><span> - </span>Google Tag Manager (avançado) </li>
                        <li><span> - </span>Google Search Console </li>

                        <li><span> - </span>Google Keyword Planner </li>
                        <li><span> - </span>Google Trends </li>
                        <li><span> - </span>Screaming Frog (ou similar) </li>
                        <li><span> - </span>SEMrush (ou similar) </li>
                        <li><span> - </span>Crazy Egg (ou similar) </li>

                        <li><span> - </span>Domínio de Excel / Google Spreadsheets </li>
                        <li><span> - </span>Developer Tools </li>
                        <li><span> - </span>Noções de Front-end (html, javascript, css) </li>

                    </ul>
                    <h3>Habilidades bônus: </h3>
                    <ul>
                        <li><span> - </span>Google Data Studio </li>
                        <li><span> - </span>Experiência + 1 ano com SEO </li>
                        <li><span> - </span>Experiência com CRO </li>
                        <li><span> - </span>Noções de Usabilidade e Experiência do Usuário
                        </li>
                        <li><span> - </span>Certificação Google Analytics Qualified Individual
                        </li>

                        <li><span> - </span>Certificação Google Tag Manager Fundamentals - Google Academy
                        </li>
                        <li><span> - </span>Google Trends </li>
                        <li><span> - </span>Screaming Frog (ou similar) </li>
                        <li><span> - </span>SEMrush (ou similar) </li>
                        <li><span> - </span>Crazy Egg (ou similar) </li>

                        <li><span> - </span>Domínio de Excel / Google Spreadsheets </li>
                        <li><span> - </span>Developer Tools </li>
                        <li><span> - </span>Noções de Front-end (html, javascript, css) </li>

                    </ul>
                    <h3>Atributos comportamentais: </h3>
                    <ul>
                        <li><span> - </span>Autogestão </li>
                        <li><span> - </span>Capacidade analítica e afinidade com números e métricas
                        </li>
                        <li><span> - </span>Boa comunicação oral e escrita </li>
                        <li><span> - </span>Curiosidade </li>
                        <li><span> - </span>Manter-se atualizado com as últimas tendências de SEO
                        </li>
                        <li><span> - </span>Facilidade de aprendizado </li>
                        <li><span> - </span>Capacidade de delegar tarefas para outros setores
                        </li>

                    </ul>
                    <h3>Duração da jornada: </h3>
                    <p>08 horas diárias - Segunda à Sexta-feira</p>
                    <h3>Intrépido aventureiro do SEO, deseja se juntar à equipe?</h3>
                    <div class="grid-70 suffix-30">
                        <div class="form-curriculo form">
                           
                                  <?php echo do_shortcode('[contact-form-7 id="810" title="Vaga analista SEO"]') ; ?>
                               
                                <!-- <div class="campo campo-input ">
                                    <input class="" type="text" placeholder="Nome" />
                                </div>
                                <div class="campo campo-input ">
                                    <input class="" type="email" placeholder="E-mail" />
                                </div>
                                <div class="campo-m campo-input ">
                                    <input class="cel" type="text" placeholder="Telefone" />
                                </div>
                                <div class="campo-m campo-input ">
                                   <div id="upload-file">
                                       <input id='input-file' type='file' value='' />
                                       <span>Anexar Currículo</span>
                                    </div>
                                                                            
                                      
                                </div>
                                <div class="campo">
                                    <div class="campo-input">
                                        <i class="icon-seta-btn-01"></i>
                                        <input type="submit" value="Iniciar Aventura" class="btn" />
                                    </div>
                                </div> -->

                         
                        </div>
                    </div>


                </div>

            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>



</main>

<?php get_footer(); ?>
