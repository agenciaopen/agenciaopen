<?php 

require_once("another-header.php"); 
if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<section class="header-categoria">
    <div class="grid-container grid-parent">
        <div class="vitrine-interna">
            <?php
            if ( has_post_thumbnail() ) { ?>

                <?php the_post_thumbnail('full'); ?>

                <?php
            }
        ?>
        </div>

    </div>
</section>
<section class="content-blog">
    <div class="grid-container grid-parent">
        <div class="grid-60 prefix-10 grid-parent">
            <div class="content-post">
                <h1>
                    <?php the_title(); ?>
                </h1>
                <hr class="l-title" />
                <div class="social">
                    <ul data-sr="li-midias">

                        <li class="b-icons-slider">
                            <a href="http://facebook.com/share.php?u=<?php the_permalink();?>&title=<?php the_title();?>" target="_blank" class="link-face" title="Compartilhar <?php the_title();?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">

										<i class="icon-facebook-01"></i>
			    					</a>
                        </li>
						
                        <li class="b-icons-slider">
                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php bloginfo('name')?>&source=LinkedIn" title="Share on Linkedin" class="link-linkedin" target="_blank" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
										<i class="icon-linkedin-01"></i>
			                    	</a>
                        </li>
                    </ul>
                </div>
                <article class="artigo">

                    <?php the_content(); ?>
     
                </article>
                <hr class="line-end" />
                   <div class="gravatar">
                      <div class="grid-100 clearfix">
                           <div class="grid-35 tablet-grid-40 relative">
                               <div class="img-gravatar">
                                 <?= get_avatar( get_the_author_meta( 'ID' ), 200 ); ?> 
                               </div>
                               <div class="social social-gravatar">
                                    <ul data-sr="li-midias">
                                        <?php 
                                           $face = get_the_author_meta('facebook', $post->post_author);
                                           if( $face != ""){ ?>
                                           
                                            <li class="b-icons-slider">
                                                <a href="<?= $face; ?>" target="_blank" title="Facebook">
                                                    <i class="icon-facebook-01"></i>
                                                </a>
                                            </li>       
                                            
                                        <?php
                                           }
                                        ?>
                                        
                                        <?php 
                                           $linkedin = get_the_author_meta('linkedin', $post->post_author);
                                           if( $linkedin != ""){ ?>
                                           
                                            <li class="b-icons-slider">
                                                <a href="<?= $linkedin; ?>" target="_blank" title="Linkedin">
                                                    <i class="icon-linkedin-01"></i>
                                                </a>
                                            </li>       
                                        <?php
                                           }
                                        ?>
                                        
                                        <?php 
                                           $twitter = get_the_author_meta('twitter', $post->post_author);
                                           if( $twitter != ""){ ?>
                                           
                                            <li class="b-icons-slider">
                                                <a href="<?= $twitter; ?>" target="_blank" title="twitter">
                                                    <i class="icon-twitter-01"></i>
                                                </a>
                                            </li>       
                                        <?php
                                           }
                                        ?>
                                        
                                        <?php 
                                           $instagran = get_the_author_meta('instagran', $post->post_author);
                                           if( $instagran != ""){ ?>
                                           
                                            <li class="b-icons-slider">
                                                <a href="<?= $instagran; ?>" target="_blank" title="instagran">
                                                    <i class="icon-instagran-01"></i>
                                                </a>
                                            </li>       
                                        <?php
                                           }
                                        ?>
                                        
                                        
                                    </ul>
                              </div>
                           </div>
                           <div class="grid-65 tablet-grid-60">
                           
                                <span class="name-author"><?= get_author_name(); ?> </span>
                                <p class="content-author"><?= get_the_author_meta('description'); ?></p>
                           </div>
                       </div>
                 </div>
                 
                 <div class="social-footer">
                     <hr class="l-title" />
                     <div class="social">
                        <ul data-sr="li-midias">

                            <li class="b-icons-slider">
                                <a href="http://facebook.com/share.php?u=<?php the_permalink();?>&title=<?php the_title();?>" target="_blank" class="link-face" title="Compartilhar <?php the_title();?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">

                                            <i class="icon-facebook-01"></i>
                                        </a>
                            </li>

                            <li class="b-icons-slider">
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php bloginfo('name')?>&source=LinkedIn" title="Share on Linkedin" class="link-linkedin" target="_blank" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                            <i class="icon-linkedin-01"></i>
                                        </a>
                            </li>
                        </ul>
                    </div>
                 </div>
                 
                
            </div>



        </div>
        <div class="grid-25 grid-parent prefix-5 tablet-grid-60 tablet-prefix-20 tablet-suffix-20 mobile-grid-90 mobile-prefix-5 mobile-suffix-5">
            <div class="aside">
                <?php get_sidebar();  ?>
            </div>

        </div>
    </div>

     


</section>
<?php 
	endwhile;
endif;
?>
<?php require_once('footer-blog.php'); ?>
