<section class="header-categoria">
    <div class="grid-container">
        <div class="lista-categorias blog-home">
            <div class="grid-30 grid-parent">
                <div class="box-blog">
                    <div class="mask"></div>
                    <div class="chamada-blog">
                        <div class="table">
                            <div class="vertical">
                                <h1>Blog da open</h1>
                                <hr />
                                <p>Coloque seus óculos de leitura e fique por dentro das novidades do mundo do marketing digital. </p>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="grid-70 grid-parent">
                <div class="grid-33 grid-parent tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria agencia-open">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/agencia-open" title="" class="cf">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-logo-open"></i>
                                    <hr />
                                    <h2>OpeNNews</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-33 grid-parent  tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria eventos">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/eventos" title="">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-calendario"></i>
                                    <hr />
                                    <h2>Eventos</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-33 grid-parent  tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria mercado">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/mercado" title="">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-megaphone"></i>
                                    <hr />
                                    <h2>Mercado</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-33 grid-parent  tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria social-media">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/social-media" title="">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-sms"></i>
                                    <hr />
                                    <h2>Social media</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-33 grid-parent  tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria tecnologia">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/tecnologia" title="">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-lampada"></i>
                                    <hr />
                                    <h2>Tecnologia</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="grid-33 grid-parent  tablet-grid-33 mobile-grid-50">
                    <div class="box-categoria variedades">
                        <a href="<?php echo get_bloginfo('url'); ?>/category/variedades" title="">
                            <div class="table">
                                <div class="vertical">
                                    <i class="icon-chat"></i>
                                    <hr />
                                    <h2>Variedades</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

      

    </div>
</section>
