<?php 

/**
 * The template name: Blog
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


get_header(); //Inicio do Loop	
if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>


<section class="header-page">
    <div class="grid-container grid-parent">
        <div class="single-thumb">
            <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('full');
            }
            ?>
        </div>
    </div>
</section>
<section class="content">
    <div class="grid-container grid-parent">
        <div class="grid-70 grid-parent">
            <div class="content-post">
                <h1>
                    <?php the_title(); ?>
                </h1>
                <hr class="l-title" />

                <article class="artigo">

                    <?php the_content(); ?>
     
                </article>
                <hr class="line-end" />
            </div>
        </div>
        <div class="grid-30 tablet-grid-60 tablet-prefix-20 tablet-suffix-20 mobile-grid-80 mobile-prefix-10 mobile-suffix-10">
            <div class="aside">
                <?php get_sidebar();  ?>
            </div>
        </div>
    </div>
</section>
<?php 
	endwhile;
endif;
?>
<?php require_once('footer-blog.php'); ?>
