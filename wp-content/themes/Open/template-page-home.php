<?php
/**
*
* Template Name: Home LP
*
*/
global $post;
$page_ID = $post->ID;
// get page ID
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/lp/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <title><?php bloginfo( 'name' ); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KSWXBK');</script>
    <!-- End Google Tag Manager -->
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KSWXBK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">
            <a href="#sobre-nos" class="menu" onclick="closeNav()">Sobre Nós </a>
            <a href="#nossos-servicos" class="menu" onclick="closeNav()">Nossos Serviços</a>
            <a href="#clientes" class="menu" onclick="closeNav()">Clientes</a>
            <a href="#depoimentos" class="menu" onclick="closeNav()">Depoimentos</a>
            <a href="https://www.instagram.com/agenciaopen_/" target="_blank" class="social-menu"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/instagram.png" alt="Instagram"></a>
            <a href="https://www.facebook.com/agenciaopen" target="_blank" class="social-menu"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/facebook.png" alt="Facebook"></a>
            <a href="https://www.linkedin.com/company/agencia-open-de-internet-e-aplicativos-web-ltda/" target="_blank" class="social-menu"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/linkedin.png" alt="LinkedIn"></a>
            <a href="https://www.agenciaopen.com/blog" target="_blank" class="social-menu"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/blog.png" alt="Blog"></a>
        </div>
      </div>
    <div class="container-fluid topo">
        <div class="row">
            <div class="col-lg-8">
                <nav class="topnav" id="myTopnav">
                    <img src="<?php bloginfo('template_url'); ?>/lp/imagens/logo-open.png" alt="Open Think Digital" class="logo">
                    <a href="#sobre-nos" class="menu">Sobre Nós </a>
                    <a href="#nossos-servicos" class="menu">Nossos Serviços</a>
                    <a href="#clientes" class="menu">Clientes</a>
                    <a href="#depoimentos" class="menu">Depoimentos</a>
                </nav>
            </div>
            <div class="col-lg-4 col-sm social">
                <a href="https://www.instagram.com/agenciaopen_/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/instagram.png" alt="Instagram" class="social"></a>
                <a href="https://www.facebook.com/agenciaopen" target="_blank"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/facebook.png" alt="Instagram" class="social"></a>
                <a href="https://www.linkedin.com/company/agencia-open-de-internet-e-aplicativos-web-ltda/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/linkedin.png" alt="Instagram" class="social"></a>
                <a href="https://www.agenciaopen.com/blog" target="_blank"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/blog.png" alt="Blog" class="social"></a>
                <span class="hamburger" onclick="openNav()">&#9776;</span>
            </div>
        </div>
    </div>
<div id="openModal" class="modalDialog">
    <div>
        <a href="#close" title="Close" class="close">X</a>
        <article class="conteudo-modal">
                <h2 class="formulario-modal">Fale com a gente</h2>
                <p class="texto-modal">Tem interesse em conhecer nossos serviços e se tornar um cliente?</p>
                <p class="texto-modal">Preencha o formulário para conversamos sobre seu projeto:</p>
        </article>
		<?php echo do_shortcode('[contact-form-7 id="1147" title="LP"]');?>
        <form style="display: none;">
            <input type="text" placeholder="Nome" required>
            <input type="email" placeholder="Email" required>
            <input type="tel" placeholder="Telefone" required>
            <input type="text" placeholder="Empresa" required>  
            <textarea placeholder="Mensagem"></textarea>
            <input type="submit" value="Enviar" id="enviar-mobile">
            </form>
    </div>
</div>
    <div class="container-fluid bg-azul principal">
        <div class="row">
            <div class="col-md-8">

                <a href="#openModal" class="footer" id="myModal">
                    <p>Enviar Mensagem</p>
                </a>
                <h1 class="titulo"><span class="novo">Novo</span><br /><span class="momento">Momento</span><br />
                <span class="nova">Nova</span><br /><span class="marca">Marca</span></h1>
                <p class="breve">E, em breve, novo site :)</p>
                <p class="saber-mais" id="borderimg3"><a href="#sobre-nos" class="saber-mais"><i class="arrow down"></i>&nbsp;Saber mais</a></p>
                <article class="conteudo" id="sobre-nos">
                    <h2 class="titulo">Sobre Nós</h2>
                    <p class="sobre-nos">Especialistas em estratégias de marketing e projetos digitais, somos pioneiros no mercado brasileiro desde 2001. Oferecemos soluções que conectam empresas a consumidores por meio de experiências 100% digitais.
                    Vivemos a transformação digital todos os dias. </p>
                    <p class="sobre-nos">Nossa equipe acompanha todas as tendências mundiais para garantir o sucesso de cada iniciativa. Por isso, trabalhamos sempre em duas frentes: Experiência do Usuário e Comportamento de Consumo. </p>
                    <h2 class="titulo" id="nossos-servicos">Nossos Serviços</h2>
                    <p class="servicos">Estratégia de conversão</p>
                    <p class="servicos">Growth Hacking</p>
                    <p class="servicos">Análise da Jornada do cliente</p>
                    <p class="servicos">Inteligência de dados</p>
                    <p class="servicos">UX Design/UX Writing</p>
                    <p class="servicos">Marketing de Conteúdo</p>
                    <p class="servicos">Social Media</p>
                </article>
            </div>
                <div class="formulario col-md-4">
                    <h2 class="formulario">Fale com a gente</h2>
                    <p class="form">Tem interesse em conhecer nossos serviços e se tornar um cliente?</p>
                    <p class="form">Preencha o formulário para conversamos sobre seu projeto:</p>
					<?php echo do_shortcode('[contact-form-7 id="1147" title="LP"]');?>
                    <form style="display:none;" >
                        <input type="text" id="nome" placeholder="Nome" required>
                        <input type="email" id="email" placeholder="Email" required>
                        <input type="tel" id="telefone" placeholder="Telefone" required>
                        <input type="text" id="empresa" placeholder="Empresa" required>  
                        <textarea id="mensagem" placeholder="Mensagem"></textarea>
                        <input type="submit" value="Enviar" id="#enviar-desktop"> 
                    </form>
            </div>
        </div>
    </div>
    <div class="container-fluid secundario">
        <div class="row">
            <div class="col-md-8 ">  
                <article class="conteudo">
                    <div class="conteudo-modal" id="clientes">
                        <h2 class="titulo">Clientes Atendidos</h2>
                        <p class="clientes">Confira as marcas que conquistaram resultados no digital com a gente:</p>
                    </div>
                    <img src="<?php bloginfo('template_url'); ?>/lp/imagens/logos.png" alt="Logos" class="logos"> 
                    <div class="conteudo-modal" id="depoimentos">
                        <h2 class="titulo">Depoimentos</h2>
                        <p class="relatos">Leia os relatos de quem realizou projetos com a gente e alcançou excelentes resultados:</p>
                    </div>
                    <div class="slideshow-container">
                        <div class="mySlides">
                            <p class="depoimento">"A Open é parceira MRV em projetos digitais há mais de 15 anos. Consolidamos uma parceria sólida e duradoura graças ao constante alinhamento, aos bons resultados conquistados e, claro, ao relacionamento e respeito mútuos."</p>
                            <p class="nome">Fabiana Afonso - Coordenadora de Marketing</p>
                            <p class="empresa">MRV</p>
                        </div>
                        <div class="mySlides">
                            <p class="depoimento">"Em parceria com a Open, conseguimos levar a Atex do nível de usuário em redes sociais para o nível de profissional em publicações e gestão do marketing digital. E o melhor é que o retorno se dá não só no campo do reforço da imagem da empresa, mas também no financeiro. É um investimento que se paga! Recomendo! "</p>
                            <p class="nome">Cristiano Drumond - Gerente Geral</p>
                            <p class="empresa">Atex</p>
                        </div>
                        
                        <a class="prev" onclick="plusSlides(-1)"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/arrow-left.png" class="seta" alt="Seta para a Esquerda"></a>
                        <a class="next" onclick="plusSlides(1)"><img src="<?php bloginfo('template_url'); ?>/lp/imagens/arrow-right.png" class="seta" alt="Seta para a Direita"></a>
                    </div>
                    <div class="dot-container">
                        <span class="dot" onclick="currentSlide(1)"></span> 
                        <span class="dot" onclick="currentSlide(2)"></span> 
                    </div>
                    <footer><img src="<?php bloginfo('template_url'); ?>/lp/imagens/logo-open.png" alt="Open Think Digital" class="logo-rodape">
                        <p class="rodape">31 3047.2931</p> <p class="rodape">contato@agenciaopen.com</p>
                        <p class="rodape"> Belo Horizonte - Minas Gerais</p><p class="rodape">&#169; Open 2020</p>
                    </footer>
                </article>
            </div>
        </div>
    </div>
	<style>
		.formulario[style*="color: grey"] span.wpcf7-form-control-wrap input, .formulario[style*="color: grey"] span.wpcf7-form-control-wrap textarea{
				color: #000;
		}
	</style>
    <script>
        var doc, bod, htm;
        addEventListener('load', function(){
         doc = document; bod = doc.body; htm = doc.documentElement;
        addEventListener('scroll', function(){
            doc.querySelector('.formulario').style.color = htm.scrollTop > 1250 ? 'grey' : 'white';
            doc.querySelector('#nome').style.backgroundColor = htm.scrollTop > 1200 ? 'white' : 'rgba(15, 27, 53, 0.1)';
            doc.querySelector('#email').style.backgroundColor = htm.scrollTop > 1150 ? 'white' : 'rgba(15, 27, 53, 0.1)';
            doc.querySelector('#telefone').style.backgroundColor = htm.scrollTop > 1150 ? 'white' : 'rgba(15, 27, 53, 0.1)';
            doc.querySelector('#empresa').style.backgroundColor = htm.scrollTop > 1050 ? 'white' : 'rgba(15, 27, 53, 0.1)';
            doc.querySelector('#mensagem').style.backgroundColor = htm.scrollTop > 1050 ? 'white' : 'rgba(15, 27, 53, 0.1)';
            doc.querySelector('#nome').style.borderBottomColor = htm.scrollTop > 1120 ? 'grey' : 'white';
            doc.querySelector('#email').style.borderBottomColor = htm.scrollTop > 1150 ? 'grey' : 'white';
            doc.querySelector('#telefone').style.borderBottomColor = htm.scrollTop > 1150 ? 'grey' : 'white';
            doc.querySelector('#empresa').style.borderBottomColor = htm.scrollTop > 1050 ? 'grey' : 'white';
            doc.querySelector('#mensagem').style.borderColor = htm.scrollTop > 1050 ? 'grey' : 'white';       
             });
        });
        var slideIndex = 1;
        showSlides(slideIndex);
        function plusSlides(n) {
          showSlides(slideIndex += n);
        }
        function currentSlide(n) {
          showSlides(slideIndex = n);
        }
        function showSlides(n) {
          var i;
          var slides = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("dot");
          if (n > slides.length) {slideIndex = 1}    
          if (n < 1) {slideIndex = slides.length}
          for (i = 0; i < slides.length; i++) {
              slides[i].style.display = "none";  
          }
          for (i = 0; i < dots.length; i++) {
              dots[i].className = dots[i].className.replace(" active", "");
          }
          slides[slideIndex-1].style.display = "block";  
          dots[slideIndex-1].className += " active"; 
        }
        function openNav() {
        document.getElementById("myNav").style.width = "100%";
        }
        function closeNav() {
        document.getElementById("myNav").style.width = "0%";
        }
        </script>
	<?php get_footer('lp'); ?>

