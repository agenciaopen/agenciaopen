$(document).ready(function () {


    $('.ativar').click(function () {
    
        if (!$(this).hasClass("ativo")) {

            $(this).parent('.acoes').find('.icon-x-01').removeClass('desativado');
            $(this).addClass('ativo');
            var str = $(this).parents('.content-box-objetivo').attr('id');
            console.log(str);

            $('.lista li.bx' + str).css('display', 'block').addClass('ativo').append("<i class=\"icon-trash-empty\"> </i>");;

        }


    });



  
    
    $("#btn-obj").on('click', function () {
         $('#area').append("Serviços indicados de acordo com os objetivos que você selecionou.");
        $("#lista-ob > li").each( function(index, value) { 
          if (($(this)).is( ".ativo" )){     
                $('#area').val($('#area').val() + "\n" + $(this).text());
             }
        });

    });
    $('.desativar').on('click', function () {
   
        $(this).parent('.acoes').find('.icon-v-01').removeClass('ativo');
        $(this).addClass('desativado');
        //  var id =  $(this).parents('.content-box-objetivo').attr('id');
        var str = $(this).parents('.content-box-objetivo').attr('id');
        console.log(str);
        // $('.lista').find('li#'+id).remove();
        $('.lista li.bx' + str).css('display', 'none');
        $('.lista li.bx' + str + ' i ').remove();

    });

    $(".lista").on('click', '.icon-trash-empty', function () {
        $(this).parents('li').css('display', "none");
    });

    $(".cel").mask("(99) 99999999?9");
    $(".tel").mask("(99) 99999999");






    $(".menu-toggle").on('click', function () {
        $(this).toggleClass("on");
        $('.menu-section').toggleClass("on");
        $("nav .hover").toggleClass('hidden');

    });

    $(".scrollLink").click(function (event) {
        event.preventDefault();
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top
        }, 500);
    });


    $("a#show-panel").click(function () {
        $("#lightbox, #lightbox-panel").fadeIn(300);

    });
    $("a#close-panel").click(function () {
        $("#lightbox, #lightbox-panel").fadeOut(300);
    })

    $('.scrollbar-inner').scrollbar();

    alturaSection();

    $(window).resize(function () {
        alturaSection();
    });

    $("a[href='#']").click(function (e) {
        e.preventDefault();
    });

    var $doc = $('html, body');
    $('#menu-header li a').click(function () {
        $(".menu-section").removeClass('on');
        $(".menu-toggle").removeClass('on');
        $("nav .hover").toggleClass('hidden');
        $doc.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 800);
        return false;


    });

    $('#hero-slider').on('init', function (e, slick) {

        var $firstAnimatingElements = $('div.hero-slide:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);
    });
    $('#hero-slider').on('beforeChange', function (e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('div.hero-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);
    });
    $('#hero-slider').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        dots: true,
        fade: true
    });

    $('#slider-depoimentos').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        dots: false,
        fade: true
    });

    $('#c-logos').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        dots: false,
        fade: true
    });



    function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function () {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function () {
                $this.removeClass($animationType);
            });
        });
    }

    window.onscroll = function () {
        headerScroll();
    };



    var header = document.getElementById("header-site");
    var headerAtivo = header.offsetTop;

    function headerScroll() {
        var url = window.location.href;
        if (window.pageYOffset > headerAtivo) {
            header.classList.add("header-ativo");
        } else {
            header.classList.remove("header-ativo");
        }
    }



});


function alturaSection() {
    var wHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    var menu = $('header').height();

    $('section').each(function () {
        var padding = parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
        var conteudo = $(this).find('.grid-container').outerHeight();
        var altura = wHeight;

        if ((conteudo) > wHeight - menu) {
            altura = conteudo;
        }


        if ($(this).hasClass('clientes')) {
            altura = altura - padding;


        }

        $(this).height(altura);

    });
}
