<?php 

/**
 * The template name: Trabalhe Conosco
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:         Agência Open
 * Data de Criação: 27/03/2018
 * Version:   2.0
 */


require_once ("another-header.php"); ?>

<main>

    <div class="header-trabalhe"></div>
    <div class="content-trabalhe">
        <div class="grid-container">
            <h1><img src="<?php bloginfo('template_url') ?>/build/img/title-trabalhe.png" alt="" /></h1>
            <div class="c-moedas">
                <span class="moeda"></span>
                <span class="moeda"></span>
                <span class="moeda"></span>
            </div>
            <div class="content-txt">
                <p>Há 18 anos nasceu a Agência Open. Essa mocinha dedicada viveu intensamente muitas aventuras e hoje, adulta, tem uma reputação louvável. Durante sua jornada venceu muitos desafios e atendeu clientes de renome como a MRV Engenharia, TOTVS, Banco BMG.</p>
                <p>Agora, como uma verdadeira veterana do marketing digital, se especializou em mais de 23 skills para atender as missões de mais de 300 clientes. Essas proezas foram conquistadas por uma equipe de pelo menos 50 bravos guerreiros e você pode fazer parte dessa party!</p>
            </div>

        </div>
    </div>
    <div class="campo-formulario">
        <div class="grid-90 prefix-10 suffix-10">
            <div class="grid-30 tablet-grid-40">
                <img src="<?php bloginfo('template_url') ?>/build/img/img-heroi.png" alt="" />
                <h2>Quer se aventurar com a gente?</h2>
                <hr />
                <h3>Pegue um copo de café, é perigoso seguir sozinho. </h3>
                <p>O Boss está do nosso lado, mas os desafios do mercado não param. Se você quer explorar o mundo digital e aumentar suas habilidades, junte-se a nós! </p>
            </div>
            <div class="grid-70 tablet-grid-60">
                <h2 class="vaga">Escolha sua <br />classe vaga:</h2>
                <ul class="lista-vagas">
                     <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-analista-cro" title="Vaga Analista SEO" id="show-panel">Analista de CRO</a>
                        <i class="mais"></i>
                    </li>
                    <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-analista-midia" title="Vaga Analista SEO" id="show-panel">Analista de Mídia</a>
                        <i class="mais"></i>
                    </li>
                    
                    <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-analista-seo" title="Vaga Analista SEO" id="show-panel">Analista de SEO</a>
                        <i class="mais"></i>
                    </li>

                     <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-de-consultor-de-novos-negocios" title="Vaga Consultor de Novos Negócios" id="show-panel">Consultor de Novos Negócios</a>
                        <i class="mais"></i>
                    </li>
                     <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-analista-de-conteudo" title="Vaga Analista de Conteúdo" id="show-panel">Analista de Conteúdo</a>
                        <i class="mais"></i>
                    </li>
                    <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-analista-de-infraestrutura" title="Vaga Analista de Infraestrutura" id="show-panel">Analista de Infraestrutura</a>
                        <i class="mais"></i>
                    </li>
                     <li>
                        <a href="<?php bloginfo('siteurl') ?>/vaga-desenvolvedor-back-end-pleno" title="Desenvolvedor(a) back-end Pleno" id="show-panel">Desenvolvedor(a) back-end Pleno</a>
                        <i class="mais"></i>
                    </li>
                   

                </ul>
            </div>
        </div>
        <div class="lista-cor">
            <span class="coracao"></span>
            <span class="coracao"></span>
            <span class="coracao"></span>
        </div>
    </div>


</main>

<?php get_footer(); ?>
